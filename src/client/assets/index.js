/** IMPORT STYLES */
import "../styles/index.scss";

/** IMPORT IMAGES */
import "./images/favicon.png";
import "./images/rectangulo.jpg";

import "./images/ferrari.jpg";
import "./images/mclaren.jpg";
import "./images/mercedez.jpg";
import "./images/ferrari2.jpg";
import "./images/m3.jpg";
import "./images/mclaren2.jpg";
import "./images/mercedezAntiguo.jpg";
import "./images/porsche.jpg";
import "./images/antiguo.jpg";

import "./images/iphone.png";
import "./images/ipad.png";
import "./images/imac.png";
import "./images/portatil.png";