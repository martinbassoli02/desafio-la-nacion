# applicants-management-system

- [Descripción del proyecto](#descripcion-del-proyecto)
- [Arquitectura](#arquitectura)
  - [Estructura de archivos](#estructura-de-archivos)
- [Comandos](#comandos)
- [Diccionario](#diccionario)
  - [hot-reload](#hot-reload)

## Descripcion del proyecto

Aplicación web que tiene como objetivo ayudar con el manejo de los postulantes a academias futuras.

Esto incluye features como
- `score system` para rankear a los postulantes
- `rechazo` de candidatos
- `preselección` de candidatos
- `disponibilidad horaria` de los candidatos.

Principales partes de la aplicación:

- Un archivo .html que representa nuestra única vista (SPA - Single Page Application).
- Estilos en archivos .css
- Funcionalidad en archivos .js
- Assets (images)

## Arquitectura

### Estructura de archivos

A nivel de directorios, tenemos lo siguiente:

```
project
│
│___src
│   │
│   └───domain
│   │   
│   └───client
│   │   │
│   │   └───assets
│   │   │
│   │   └───markup
│   │   │
│   │   └───styles
│   │   │
│   │   └───views
│   │   │   index.html
│   │   index.js
│   package.json
│   package-lock.json
│   webpack.config.json
│   .gitignore
│   README.md
```

Para que la aplicación compile y corra configuramos webpack para levantar el servidor. Podés ver la la sección donde se explica la configuración de webpack: `webpack.config.js`.

**/src**

Carpeta que contiene todo el código del proyecto.

**/src/domain**

Carpeta que contiene todas las reglas y funcionalidades vinculadas negocio.

**/src/client**

Carpeta que contiene todo lo relacionado a la interface de usuario.

**/src/client/assets**

Carpeta que contiene assets externos como
- Imágenes
- Listado de candidatos en un archivo .xslx

Tambien ayuda a webpack a traquear estos assets para poder utilizarse en los archivos .js. (src/client/assets/index.js)

**/src/client/styles**

Carpeta que contiene los estilos de la UI.

**/src/client/views**

Carpeta que contiene las vistas html del proyecto. Al ser un SPA, tiene solo un file .html.

**./src/index.js**

Archivo que
- inicializa la app.
- Da la funcionalidad de cargar el programa.
- Inicializa la interface de usuario.
- Etc.

**package.json**

Archivo que se crea a partir del comando npm init -y cuya finalidad es brindar información sobre el proyecto. Esto incluye las dependencias, comandos (arrancar el server, correr los tests), author, repo asociado; etc.

**package-lock.json**

Archivo que se crea a partir del comando npm init -y cuya finalidad es brindar información sobre el proyecto. Esto incluye las dependencias, comandos (arrancar el server, correr los tests), author, repo asociado; etc.

**.gitignore**

Archivo que nos permite ignorar ciertos directorios/archivos para que no sean tenidos en cuenta por GIT. De esta manera estos archivos no son subidos al repo.

**README.md**

Documentación que presenta el proyecto. Este archivo donde se escribe la explicación del proyecto. 

**webpack.config.js**

Para poder utilizar webpack en nuestro proyecto, primero instalamos las 3 dependecias para utilizarlo:

```
npm install --save-dev webpack webpack-cli webpack-dev-server
```

Esto agrega en el listado de devDependencies, los 3 packages agregados:

- `webpack`: Dependencia que contiene el core de webpack.
- `webpack-cli`: Dependencia que nos permite usar comandos de webpack en la terminal.
- `webpack-dev-server`: Dependencia que nos permite levantar la aplicación con [hot-reload](#hot-reload)

`./package.json`
```
"devDependencies": {
  "webpack": "^5.70.0",
  "webpack-cli": "^4.9.2",
  "webpack-dev-server": "^4.7.4"
}
```

**Análisis de webpack.config.js paso a paso**

Para lograr que nuestra aplicación funcione correctamente con las características mencionadas en la sección donde se [describe el proyecto](#descripcion-del-proyecto) tenemos que:

1. Tener un HTML que represente nuestra app.
2. Compilar los archivos de estilos (.css / .scss)
3. Compilar el código de JS (.js)
4. Insertar en el HTML los estilos y el código JS
5. Trackear la ubicación de los assets/images para poder usarlos en el HTML

Todo esto hacemos configurando webpack:

`./webpack.config.js`
```
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const BUILD_DIR = path.join(__dirname, 'public/dist');

module.exports = {
  entry: {
    index: './src/index.js',
    assets: './src/client/assets/index.js',
  },
  output: {
    path: BUILD_DIR,
    filename: "[name].js",
    assetModuleFilename: "assets/images/[name][ext]",
    clean: true
  },
  plugins: [
    new HtmlWebpackPlugin({
        template: './src/client/views/index.html',
        chunks: ['index', 'assets'],
        inject: true,
        filename: 'index.html'
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css'
    }),
  ],
  module: {
    rules: [
      {
        test: /\.(css)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          "css-loader"
        ]
      },
      {
        test: /\.(scss)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          "css-loader",
          "sass-loader",
        ]
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource'
      },
    ]
  }
}
```

1. Tener un HTML que represente nuestra app.

No existe aplicación web sin HTML. Para esto necesitamos:

- Crear un archivo `index.html`
- Insertar en ese HTML los `assets` externos que necesite: `css` y `js`.

La creación del archivo la encontramos en `./src/views/index.html`

Para insertar en el HTML el código css y js, necesitamos instalar un plugin `HtmlWebpackPlugin` que nos permite trackear los distintos html que tengamos en el proyecto para poder insertarles los archivos compilados que necesitemos de `css` y `js`:

`./webpack.config.js`
```
  const HtmlWebpackPlugin = require('html-webpack-plugin');
  ...
  plugins: [
    new HtmlWebpackPlugin({
        template: './src/client/views/index.html',
        ...
        filename: 'index.html'
    }),
    ...
  ]
```

- `const HtmlWebpackPlugin = require('html-webpack-plugin');`: Importamos el plugin para trackear el HTML.
- `plugins: [...]`: Definimos los plugins que va a usar webpack al compilar la aplicación.
  - `new HtmlWebpackPlugin({})`: Instanciamos el plugin de HTML. Recibe un objeto de configuración {} con la siguiente forma:
    - `template: './src/client/views/index.html',`: Path hacia el HTML que queremos que webpack modifique.
    - `filename: 'index.html'`: Lo que hará webpack con el HTML que le aclaramos en `template` es parsearlo, agregarle los `chunks` y generar un html nuevo con el nombre de esta propiedad `filename`. En este caso ese nombre será `index.html`

Para utilizar el plugin `html-webpack-plugin` necesitamos instalarlo:

```
npm install --save-dev html-webpack-plugin
```

Se agrega esta dependencia al listado de devDependencias:

`./package.json`
```
"devDependencies": {
  "html-webpack-plugin": "^5.5.0",     <---
  "webpack": "^5.70.0",
  "webpack-cli": "^4.9.2",
  "webpack-dev-server": "^4.7.4"
}
```

2. Compilar los archivos de estilos (.css / .scss)

Mencionamos en otros puntos ateriores que parte del trabajo de webpack va a ser insertar las hojas de estilo a nuestro HTML.

Para lograr esto, tenemos que decirle a webpack
- El lugar donde encontrar los archivos con extensión `.css` y `.scss`
- Que compile estos archivos en archivos `.css` (los archivos `.scss` son compilados a `.css`)
- Que inserte esos archivos compilados al HTML.

Para cada tipo de archivo, usamos uno o más loader/s específicos.

Para el caso de archivos de estilos en el proyecto utilizamos:

- `css-loader`: Permite entender los `import` que se hacen en archivos js.
- `sass-loader`: Transforma los archivos `sass (.scss)` que encuentra a archivos `.css` que el navegador puede interpretar.
- `loader de mini-css-extract-plugin`: Crea archivos .css a partir de los archivos `.css` y `.scss` compilados para insertar en el html.

Ejemplo de `css-loader` en `[el index.js dentro de la carpeta /assets](/src/client/assets/index.js)`
```
import "../styles/index.scss";
```

`./webpack.config.js`
```
...
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
...

module.exports = {
  entry: {
    ...
    assets: './src/client/assets/index.js',
    ...
  },
  output: {
    path: BUILD_DIR,
    filename: "[name].js",
  },
  plugins: [
    ...
    new MiniCssExtractPlugin({
      filename: '[name].css'
    })
  ],
  ...
  module: {
    rules: [
      {
        test: /\.(css)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          "css-loader"
        ]
      },
      {
        test: /\.(scss)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          "css-loader",
          "sass-loader",
        ]
      },
      ...
    ]
  }
```

Para compilar los archivos .css y .scss primero hay que decirle a webpack donde buscar:

- `entry: {}`: Le dice a webpack donde buscar. Cada propiedad de este objeto será un entrypoint distinto. Podemos tener tantos entrypoints como queramos.
  - `assets: './src/client/assets/index.js'`: Entrypoint para las hojas de estilos: Path al archivo `index.js` que utiliza (importa) los estilos de `index.scss`. Por esta razón, todos los archivos de estilos que creemos en la carpeta de `styles` tienen que importarse en este `index.scss`. Para que webpack los mapee e incluya en el archivo output final de estilos `assets.css`.

Luego le decimos como compilar esos archivos encontrados:

- `const MiniCssExtractPlugin = require('mini-css-extract-plugin');`: Importamos el plugin para utilizarlo.

- `plugins: [...]`: Definimos los plugins que va a usar webpack al compilar la aplicación.
  - `new MiniCssExtractPlugin({})`: Instanciamos el plugin. Recibe un objeto de configuración {} con la siguiente forma:
    - `filename: '[name].css'`: Nombre del archivo que va a tener la hoja de estilo a agregar en el html.
- `module: {}`: Es una propiedad configuración de webpack. module = archivo. En esta sección vamos a decirle a webpack la manera de procesar cada archivo `.css`, `.scss`, `.js`, `.png`, etc.
  - `rules: []`: Establecemos las reglas a seguir para cada archivo trackeado por webpack. En nuestro proyecto las reglas que escribis hacen referencia a los loaders que webpack va a tener que aplicarle a cada archivo. Es un array que recibe un objeto para cada regla que establezcamos. Este objeto tiene 2 propiedades clave:
    - `test`: Es una regular expression que le dice a webpack a que archivos aplicar esta regla.
    - `use`: Es un array con todos los loaders que se van a aplicar a los archivos que matcheen con la regular expression de `test`

*Tanto para los archivos `.css` como `.scss`, les aplicamos `{ loader: MiniCssExtractPlugin.loader }` y `css-loader`.*
*Solo a los archivos `.scss` le aplicamos `sass-loader`.*

Por último le decimos a webpack que hacer con los archivos compilados:

- `output: {}`: En esta sección le decimos a webpack información sobre los archivos compilados: bajo que directorio colocarlos, que nombres ponerles, etc.  Producirá tantos output files como entrypoints le hayamos dado. En nuestro caso tendremos un output file para el css que se llamará `assets.css` y otro para el js. que se llamará `index.js`.
  - `path: BUILD_DIR,`: Path donde queremos poner los archivos compilados
  - `filename: "[name].js",`: Nombre de los archivos compilados.

Recordar siempre instalar los loaders que se utilicen. Para esta sección para hojas de estilos instalamos: `css-loader`, `sass`, `sass-loader`, `mini-css-extract-plugin`:

```
npm install --save-dev css-loader sass sass-loader mini-css-extract-plugin
```

Se agrega las 4 dependencias instaladas al listado de devDependencias:

`./package.json`
```
"devDependencies": {
  "css-loader": "^6.7.0",                <---
  "html-webpack-plugin": "^5.5.0",
  "mini-css-extract-plugin": "^2.6.0",   <---
  "sass-loader": "^12.6.0",              <---
  "sass": "^1.51.0",                     <---
  "webpack": "^5.70.0",
  "webpack-cli": "^4.9.2",
  "webpack-dev-server": "^4.7.4"
}
```

3. Compilar el código de JS (.js)

Para compilar el código de JS no nos hace falta ningun loader.

Solo tenemos que especificarle donde encontrar los archivos `.js` y que hacer con el archivo output compilado:

```
const path = require('path');
const BUILD_DIR = path.join(__dirname, 'public/dist');

module.exports = {
  entry: {
    ...
    index: './src/index.js',
    ...
  },
  output: {
    path: BUILD_DIR,
    filename: "[name].js",
    ...
  }
  ...
}
```

- `entry: {}`: Le dice a webpack donde buscar. Cada propiedad de este objeto será un entrypoint distinto. Podemos tener tantos entrypoints como queramos.
  - `index: './src/index.js',`: Path hacia el archivo index.js que tiene todo el código JS. Fucniona como entrpoint del JS en la app.
- `output: {}`: En esta sección le decimos a webpack información sobre los archivos compilados: bajo que directorio colocarlos, que nombres ponerles, etc.  Producirá tantos output files como entrypoints le hayamos dado. En nuestro caso tendremos un output file para el css que se llamará `assets.css` y otro para el js. que se llamará `index.js`.
  - `path: BUILD_DIR,`: Path donde queremos poner los archivos compilados. Se colocará en la carpeta /public/dist al mismo nivel que donde se encuentra este archivo de configuración
  - `filename: "[name].js",`: Nombre de los archivos compilados. Para el caso de JS será `index.js`

4. Insertar en el HTML los estilos y el código JS

Una vez tenemos compilado el código .js y .css, lo insertamos en el HTML:

`./webpack.config.js`
```
  const HtmlWebpackPlugin = require('html-webpack-plugin');
  ...
  plugins: [
    new HtmlWebpackPlugin({
        ...
        inject: true,
        chunks: ['index', 'assets'],
        ...
    }),
    ...
  ]
```

- `chunks: ['index', 'assets'],`: Vamos a insertar estos chunks (output o archivos compilados .css y .js) en el HTML.
  - `index`: Es el archivo compilado final que sale del entrypoint `index`. Va a contener todo el código js.
  - `assets`: Es el archivo compilado final que sale del entrypoint `assets`. Contendrá todos los estilos.

5. Trackear la ubicación de los assets/images para poder usarlos en el HTML

Para poder utilizar las imagenes en el html, tenemos que colocarlas con un nombre predecible en un directorio desde donde podamos importarlas:

Para eso agregamos a la configuración:

`./webpack.config.js`
```
module.exports = {
  output: {
    ...
    assetModuleFilename: "assets/images/[name][ext]",
    ...
  },
  module: {
    rules: [
      ...
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource'
      },
    ]
  }
}
```

- `output: {}`
  - `assetModuleFilename: "assets/images/[name][ext]",`: Le decimos que a todos los assets que encuentre, los coloque en este path `assets/images` y a cada uno les ponga el nombre `[name]` (el mismo con el cual los encuentra) con la extensión que tienen `[ext]`.

La pregunta que sale de esta linea es: Webpack, ¿qué considera asset?.

Para decirle a webpack que es un asset agregamos otra regla dentro de rules:

- `module: {}`
  - `rules: []`. La nueva regla será un objeto con estas propiedades
    - `test: /\.(png|svg|jpg|jpeg|gif)$/i,`: Establecemos que archivos son assets.
    - `type: 'asset/resource'`: Establecemos que los archivos anteriores deben tratarse como assets
    },
  ]
}

*Aclaraciones:*
- `output: {}`
  - `clean: true`: Esto se hace para que cada vez que corramos npm run build para ver como esta compiladon los archivos webpack, este borre la carpeta public/dist antes de compilar la app. Es una forma de tener un start limpio cada vez que buildeamos (corremos npm run build) la app.

## Comandos

Los comandos estan listados en el package.json:

```
...
"scripts": {
  "build": "npx webpack",
  "dev-server": "webpack-dev-server --config webpack.config.js --mode development --open"
},
...
```

- Para iniciar el entorno de desarrollo y poder ver los cambios que vamos haciendo al código:

```
npm run dev-server
```

Esto levanta 

- Para visualizar como webpack compila la aplicación, con todo el output de archivos que produce:

```
npm run build
```

## Diccionario

### hot reload

El hot reload nos permite tener una aplicación que se refresque automáticamente al detecar cambios en los archivos.