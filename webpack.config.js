const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const BUILD_DIR = path.join(__dirname, 'public/dist');

module.exports = {
  entry: {
    index: './src/index.js',
    assets: './src/client/assets/index.js',
  },
  output: {
    path: BUILD_DIR,
    filename: "[name].js",
    assetModuleFilename: "assets/images/[name][ext]",
    clean: true
  },
  plugins: [
    new HtmlWebpackPlugin({
        template: './src/client/views/index.html',
        chunks: ['index', 'assets'],
        inject: true,
        filename: 'index.html'
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css'
    }),
  ],
  module: {
    rules: [
      {
        test: /\.(css)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          "css-loader"
        ]
      },
      {
        test: /\.(scss)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          "css-loader",
          "sass-loader",
        ]
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource'
      },
    ]
  }
}